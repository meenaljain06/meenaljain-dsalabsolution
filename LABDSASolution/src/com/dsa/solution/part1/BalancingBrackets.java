package com.dsa.solution.part1;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

public class BalancingBrackets {

    public static boolean areBracketsBalanced(String str){
        if(str.length() == 0){
            return false;
        }
        HashMap<Character,Character> bracket = new HashMap<>();
        bracket.put('(',')');
        bracket.put('{', '}');
        bracket.put('[', ']');

        Stack<Character> stack = new Stack<Character>();
        for(int i=0; i<str.length(); i++){
            char c = str.charAt(i);
            if(c == '(' || c=='{' || c=='['){
                char closingBracket = bracket.get(c);
                stack.push(closingBracket);
            } else if(!stack.isEmpty()){
                char topChar = stack.peek();
                if(topChar == c)
                    stack.pop();
                else
                    return false;
            } else {
                return false;
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter input string");
        String input = scanner.next();
        if(areBracketsBalanced(input)){
            System.out.println("The entered string has balanced brackets");
        } else {
            System.out.println("The enetered string do not contain Balanced brackets.");
        }
    }
}
